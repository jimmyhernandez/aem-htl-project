use(function(){
    var pageName = currentPage.name;
    var title = currentPage.properties.get("jcr:title");
    var resourceName = granite.resource.name;
    var resourceTitle = properties.get("jcr:title");

    var val1 = this.val1;
    var val2 = this.val2;

    return {
        pageName: pageName,
        title: title,
        resourceName: resourceName,
        resourceTitle: resourceTitle,
        vals: this.val1 + " " + this.val2
    };
});